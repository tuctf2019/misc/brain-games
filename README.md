# Brain Games

## Challenge description
Time to go back to the basics... you remember those, right?

## Required files for hosting
`braingame.py`

## Hosting instructions
- The python file in included with the docker image.
- To run without using our included images (recommended for this challenge), just setup a listener that runs the python script when it gets a connection

## Possible issues & hints
- The screen should scale to your shell window size, this was not the case during the CTF and the rendered window was a fixed size.
- There should NOT be a timeout when connected to this challenge. There were some issues with this with how we had out infrastructure setup, so hosting via other means outside the docker image is recommended.
- There are 5 incorrect answers before it kicks you out; you know if you answered incorrectly by the face on the left looking angry.
- For base conversion, octal answers must have a leading 0. E.g. 234 would be sent as 0234 and 36 would be sent as 036.

## Flag 
`TUCTF{7H3_M0R3_Y0U_KN0W_G1F}`
