Brain Games -- TUCTF 2019

A Note: Due to how we ran our infrastructure this year, this challenge did not function as intended and had two things wrong with it.
There was no intended timeout, and as this was written in python asciimatics, the game window was meant to resize tiself to your terminal window.
Intended behavior can be seen just by running the released source script.

Brain games is a parody of the old DS title Brain Age. The face on the left of the screen is happy when you answer correctly, and angry when you answer incorrectly.
There are 3 levels that can be done in any order:

Level 1 -- Converting between binary/octal/decimal/hexadecimal
-----------------------------------------------------------------
    This level requires you to get 20 questions correct to "win", with 5 incorrect answers before it kicks you out and you must start the level over. 
    A number and two bases are randomly chosen, and the user must input the correct converted number. 
    Due to how python deals with octal numbers, however, a leading zero MUST be put in front of the correct answer.
    Eg. 234 must be 0234, 36 must be 036, etc. 

Level 2 -- Bitwise operations (AND, OR, XOR)
    This level requires you to get 20 questions correct to "win", with 5 incorrect answers before it kicks you out and you must start the level over. 
    Two numbers and a bitwise operation (AND -> &, OR -> |, XOR -> ^) are randomly chosen, and the user must input the correct integer answer (in base 10). 

Level 3 -- Virus History
    This level has 20 questions related to computer virus history. The questions are given in the same order, and the 4 multiple choise answers shown are chosen randomly from the correct answer bank.
    The correct answer just needed to be retyped as input. Below is the answer key.
            | Question                                                                                                                                               |   Answer
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | First worm to spread over the Internet, and the first well known use of buffer overflows.                                                              |   Morris Worm
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | The best known of the early macro viruses, mass-mailed itself and caused est. $80 million in damages.                                                  |   Melissa Virus
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | First known virus to erase flash ROM BIOS data, activates on April 26th.                                                                               |   CIH Virus
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | Mass-mailer virus with a VBScript attachment, considered one of the most damaging worms ever.                                                          |   ILOVEYOU Worm
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | Exploited Windows services to spread over a network in 2003, DDoSed Windows Update.                                                                    |   Blaster Worm
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | Exploited Windows LSASS, spread over interconnected networks. Removed MyDoom and Bagle in 2004.                                                        |   Sasser Worm
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | Bridged an airgap to stealth takedown Iranian nuclear development in 2010.                                                                             |   Stuxnet
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | A trojan which marked the start of the 'ransomware era' in 2013.                                                                                       |   CryptoLocker
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | A botnet which spread over the Internet of Things to commit large-scale DDoS attacks from 2016 onward.                                                 |   Mirai
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | Ransomware which utilized leaked NSA exploits that launched a global attack on Windows computers.                                                      |   WannaCry
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | This malicious command from 1974 eats system resources until a crash occurs.                                                                           |   Forkbomb
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | Non-malicious DOS virus that infects files when present in memory. Payload forces all on-screen characters to fall down.                               |   Cascade
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | This virus in 2003 exploited a buffer overflow in Microsoft SQL Server and took less than 10 minutes to reach 90% of vulnerable computers worldwide.   |   Slammer Worm
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | An early botnet which did no initial damage other than block antivirus updates in 2008/2009.                                                           |   Conficker
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | A MS-DOS virus which, when run, infects every DOS executable in the current directory. Plays a tune that lasts approximately 1:49.                     |   Techno
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | Spyware best known for its purple ape mascot. The software is also classified as adware, and was sued twice.                                           |   BonziBUDDY
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | One of the only known viruses for the Solaris operating system, developed in 1998.                                                                     |   Solar Sunrise
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | A rogue antivirus program which simulates a malware infection after several weeks running 'unactivated'.                                               |   Navashield
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | One of the firse known viruses, spread through the ARPANET. Infected systems display the message I'M THE CREEPER: CATCH ME IF YOU CAN.                 |   Creeper
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
            | The first nematode, created to fight infections of Creeper.                                                                                            |   Reaper
            | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
